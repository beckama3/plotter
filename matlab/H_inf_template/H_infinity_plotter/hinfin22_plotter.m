% clc
% clear all
%C:\Users\becka\Documents\Me_dokumenty\CVUT_FEjL\DIPLOMKA_V2\matlab


 %% step 4 - Choosing weihting filters
 % tuning parameters

s=tf('s');
ws1 = (0.66*s+10)/(s+1000);
ws2 = (0.66*s+10)/(s+1000);

% regulation error e_j weights
Ws=[ws1,  0
        0   ws2];
    
% ws1 = (0.66*s+10)/(s+1000);
% ws2 = (0.66*s+10)/(s+1000);
    
%control signal weights
Wu = 0.001*eye(2); % Weights.
% Wu=[0,  0
%         0   0];

%% Step 5 - Making generalized plant model
% hinfinity is the simulink model
[A1, B1, C1, D1]=linmod('hinfinity_plotter');
P=ss(A1, B1, C1, D1);
%% generalized Plant is ready stored in P
[K,CL,GAM] = hinfsyn(P,2,2);

% %% K is the required H infinity Controller
% sigmaplot(K)
% figure
% %P=augw(G,Ws,Wu)
% %[K,CL,GAM]=hinfsyn(P);
% step(feedback(G*K, eye(2)))