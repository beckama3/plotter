bode(lin_plotter_sys_reduced), grid

%% Structure of regulation
C1 = tunablePID('C1','pid');
C21 = tunablePID('C21','pid');
C12 = tunablePID('C12','pid');
C2 = tunablePID('C2','pid');

%% Weighting functions
%Skogestad - general design figure 3.19 page 108, see also page 360
s = tf('s');

% WU to penalize large inputs (control action) - also known as W_3
%c an be constant
WU = tf(0.01);



% WT for robustness and to avoid sensivity to noise - high pass filter - also known as W_2
% template corresponds to guidelines of ORR course

% r_0 = 0.1; %relative uncertainity on low frequencies
% r_inf = 2; %relative uncertainity on high frequencies
% freq = 600; %in radians
% theta = 1 / freq; %1/theta is the frequency, where the relative uncertainty reaches 100 percent
% WT = theta*s+r_0 / (theta/r_inf)*s+1;
%
% wc = 5000;
% WT = 0.1*(1+0.001*s/wc)/(0.001+s/wc);
% WT = (s/(s+1));
WT=tf(0.1);





% WP for performance - weighting the control error - also known as W_1
% template corresponds to guidelines of ORR course
M = 2;% coefficient "how much do i want to overshoot" 2
omegastar_B = 10; %do jake frekvence az bude ten regulator prispivat, regulovat, potlacovat disturbance
A = 0.01; %zeslabeni na sirce pasma je utlumeni disturbance koef A
WP = (s/M + omegastar_B)/(s + omegastar_B*A);
% 
% WP =  1*(1+0.001*s/wc)/(0.001+s/wc);
% 
% wc = 500;  % target crossover
% s = tf('s');
% LS = 1*(1+0.001*s/wc)/(0.001+s/wc);
% %LS=tf(1);
% 
% %wc = 50000;
% TS = 1*(0.66*s+10)/(s+1000);

%% Specify & label the block I/Os
Wu1 = WU;   Wu1.u = 'T1';  Wu1.y = 'uw1';
Wu2 = WU;   Wu2.u = 'T2';  Wu2.y = 'uw2';

Wt1 = WT;   Wt1.u = 'omega_1';  Wt1.y = 'omegaw1';
Wt2 = WT;   Wt2.u = 'omega_2';  Wt2.y = 'omegaw2';

Wp1 = WP;    Wp1.u = 'ce1';   Wp1.y = 'cew1';
Wp2 = WP;    Wp2.u = 'ce2';   Wp2.y = 'cew2';

%tuneable PIDs
C1.u = 'e1';   C1.y = 'u1';
C12.u = 'e1';   C12.y = 'u12';
C21.u = 'e2';   C21.y = 'u21';
C2.u = 'e2';   C2.y = 'u2';



% Specify summing junctions
Sum_e1 = sumblk('e1 = -ce1');
Sum_e2 = sumblk('e2 = -ce2');

Sum_1 = sumblk('T1 = u1 + u21');
Sum_2 = sumblk('T2 = u2 + u12');

Sum_ce1 = sumblk('ce1 = r1 + omega_1');
Sum_ce2 = sumblk('ce2 = r2 + omega_2');


%% Connect everything together to get generalized plant

T0 = connect(lin_plotter_sys_reduced, Wu1, Wu2, Wt1, Wt2, Wp1, Wp2, C1, C12, C21, C2, Sum_1, Sum_2, Sum_e1, Sum_e2,Sum_ce1,Sum_ce2,{'r1','r2'}, {'uw1','uw2','omegaw1','omegaw2','cew1','cew2'});

%with weights on torque inputs:
%T0 = connect(lin_plotter_sys_reduced, We1, We2, Wt1, Wt2, C1, C12, C21, C2, Sum_1, Sum_2, Sum_e1, Sum_e2,{'r1','r2'}, {'omega_1','omega_2','ew1','ew2','tw1', 'tw2'});
%% H inf optimalization
rng('default')
opt = hinfstructOptions('Display','final','RandomStart',10);
Tt = hinfstruct(T0,opt);

C1_final = getBlockValue(Tt,'C1')
C1_final.Kp = 1;
C12_final = getBlockValue(Tt,'C12')
C21_final = getBlockValue(Tt,'C21')
C2_final = getBlockValue(Tt,'C2')
C2_final.Kp = 1;
