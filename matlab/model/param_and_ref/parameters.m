%% Theta reference
%with T you can tune how fast the square should be drawed
T = 1;

fs = 1000;
t = 0 : 1/fs : 2*T-1/fs;

signal_ref = 40*(sawtooth(pi*2/T*t,1/2)+1);
signal_ref = signal_ref(2*T*fs/16+1 : T*fs+2*T*fs/16) - 20;

signal_ref1 = flip(signal_ref);

t = 0:1/fs:T-1/fs;

%this is necessary for the memory block to work
ref_plus_one = zeros(1,length(signal_ref)+1);


%%
r1 = 0.0194;
r2 = r1;
r3 = r1;
r4 = r1;
r5 = r1;
r6 = r1;
r7 = r1;
r8 = r1;
%% mass / moment inertia
I1 = 5.6e-5;
I2 = I1;
I3 = I1;
I4 = I1;
I5 = I1;
I6 = I1;
I7 = I1;
I8 = I1;

%% belt stiffness
k1 = 1.781e4;
k2 = k1;
k3 = 2.5013e4;
k4 = k3;
k5 = 1.2565e5;
k6=k5;
k7 = 8.4456e4;
k8 = k7;
k9 = 2.2585e4;

%% friction coefficients
b1 = 0.0021;
b2 = b1;
b3 = b1;
b4 = b1;
b5 = b1;
b6 = b1;
b7 = b1;
b8 = b1;
%%
mx = 0.446;
my = 4.07;%4.07;

%%friction coefficients
bx = 23.2224;
by = 46.4449;
%%
% %%
% syms dw1 dw2 dw3 dw4 dw5 dw6 dw7 dw8 ddx ddy dq13 dq37 dq7x dq48 dq8x dq24 dq26 dq56 dq15 T1 T2 w1 w2 w3 w4 w5 w6 w7 w8 dx dy vx vy th1 th2 th3 th4 th5 th6 th7 th8 x y
% 
% 
% dth1 = w1;
% dth2 = w2;
% dth3 = w3;
% dth4 = w4;
% dth5 = w5;
% dth6 = w6;
% dth7 = w7;
% dth8 = w8;
% vx = dx;
% vy = dy;
% 
% q15 = r5*th5 - r1*th1 + y;
% q13 = r1*th1 - r3*th3;
% q37 = r3*th3 + r7*th7 - y;
% q7x = r7*th7-x;
% q8x = x - r8*th8;
% q48 = -r8*th8 - r4*th4 - y;
% q24 = r4*th4-r2*th2;
% q26 = r2*th2+r6*th6+y;
% q56 = r6*th6 - r5*th5;
% 
% dw1 = 1/I1*(T1-b1*w1-r1*k1*q13+ r1*q15*k3);
% 
% dw2 = 1/I2*(T2-b2*w1+r2*k2*q24- r2*q26*k4);
% 
% dw3 = 1/I3*(T1-b1*w1-r1*k1*q13+ r1*q15*k3);
% dw4 = 1/I4*(T1-b1*w1-r1*k1*q13+ r1*q15*k3);
% dw5 = 1/I5*(T1-b1*w1-r1*k1*q13+ r1*q15*k3);
% dw6 = 1/I6*(T1-b1*w1-r1*k1*q13+ r1*q15*k3);
% dw7 = 1/I7*(T1-b1*w1-r1*k1*q13+ r1*q15*k3);
% dw8 = 1/I8*(T1-b1*w1-r1*k1*q13+ r1*q15*k3);
% 
% 
% 
% dp1 = (T1 - b1*w1+r1)/I1


%%
% P = 100;
% D =0.01;
% close all
% step = 0.3;
% xref = [0 0.10 0.10 -0.10 -0.10 0.10]';
% yref = [0 0.10 -0.10 -0.10 0.10 0.10]';
%   sim('model',7*step) 
% %draw
% figure
% plot(xref,yref,'linewidth',1.5)
% grid on
% xlim([-0.2 0.2])
% ylim([-0.2 0.2])
% hold on
% plot(ans.simout.Data,ans.simout1.Data,'--','LineWidth',1.5)
% axis equal