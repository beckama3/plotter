% A = linsys1.A;
% B = linsys1.B;
% C = linsys1.C;
% D = linsys1.D;

A = lin_plotter_sys_reduced.A;
B = lin_plotter_sys_reduced.B;
C = lin_plotter_sys_reduced.C;
D = lin_plotter_sys_reduced.D;

[num_11, den_11] = ss2tf(A, B(:,1), C(1,:), D(1,1));
[num_12, den_12] = ss2tf(A, B(:,2), C(1,:), D(1,2));
[num_21, den_21] = ss2tf(A, B(:,1), C(2,:), D(2,1));
[num_22, den_22] = ss2tf(A, B(:,2), C(2,:), D(2,2));

G_11 = minreal(tf(num_11, den_11));
G_12 = minreal(tf(num_12, den_11));
G_21 = minreal(tf(num_21, den_21));
G_22 = minreal(tf(num_22, den_22));

%G = [G_11 G_12; G_21 G_22];
G_reduced = [G_11 G_12; G_21 G_22];