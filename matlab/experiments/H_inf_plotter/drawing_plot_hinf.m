clf 

hight = 10;
width = 11;
figure(1)
set(gcf, 'Units', 'centimeters')
set(gcf, 'Position', [8 8 width hight])
set(gcf, 'PaperPositionMode', 'auto')

plot(out.simout, out.simout1,'LineWidth',1);
%title('Drawing head pattern');
xlabel('Position x') 
ylabel('Position y') 
xlim([-1.1 0.1])
ylim([-0.6 0.6])

ax = gca;

%% detail
clf 

hight = 10;
width = 11;
figure(1)
set(gcf, 'Units', 'centimeters')
set(gcf, 'Position', [8 8 width hight])
set(gcf, 'PaperPositionMode', 'auto')

plot(out.simout, out.simout1,'LineWidth',1);
%title('Drawing head pattern');
xlabel('Position x') 
ylabel('Position y') 
xlim([-0.99 -0.94])
ylim([-0.505 -0.445])

ax = gca;

%set(gca,'XTick',[],'YTick',[])
%% detail on last iteration
clf 

hight = 10;
width = 11;
figure(1)
set(gcf, 'Units', 'centimeters')
set(gcf, 'Position', [8 8 width hight])
set(gcf, 'PaperPositionMode', 'auto')

plot(out.simout(305000:322000), out.simout1(305000:322000),'LineWidth',1);
%title('Drawing head pattern');
xlabel('Position x') 
ylabel('Position y') 
xlim([-0.99 -0.94])
ylim([-0.505 -0.445])

ax = gca;

%set(gca,'XTick',[],'YTick',[])

%% quadratic performance



clf

hight = 11;
width = 25;
figure(1)
set(gcf, 'Units', 'centimeters')
set(gcf, 'Position', [8 8 width hight])
set(gcf, 'PaperPositionMode', 'auto')

% hold on
box on
% tstart = 98.5;
% tend = 100;
semilogy(out.tout, out.simout6+out.simout7);
grid on
%title('Drawing head pattern');
xlabel('time [s]') 
ylabel('amplitude [-]') 

%% Control action


clf
box on 

t = 0:0.001:6;

hight = 11;
width = 25;
figure(1)
set(gcf, 'Units', 'centimeters')
set(gcf, 'Position', [8 8 width hight])
set(gcf, 'PaperPositionMode', 'auto')

hold on
plot(out.tout, out.simout4,'LineWidth',1);
plot(out.tout, out.simout5,'LineWidth',1);
%title('Drawing head pattern');
xlabel('t [s]') 
ylabel('Position') 
xlim([0 4])
ylim([-30 30])

legend('motor 1','motor 2')

ax = gca;
ylabel('amplitude [-]') 