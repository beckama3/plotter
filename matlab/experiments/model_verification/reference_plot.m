clf
box on 

hight = 10;
width = 11;
figure(1)
set(gcf, 'Units', 'centimeters')
set(gcf, 'Position', [8 8 width hight])
set(gcf, 'PaperPositionMode', 'auto')

hold on
plot(out.t, out.simout2,'LineWidth',1);
plot(out.t, out.simout3,'LineWidth',1);
%title('Drawing head pattern');
xlabel('t [s]') 
ylabel('Position') 
xlim([0 4])
ylim([-30 100])

legend('motor 1','motor 2')

ax = gca;

%set(gca,'XTick',[],'YTick',[])