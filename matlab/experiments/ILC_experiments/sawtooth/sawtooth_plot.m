clf 

hight = 10;
width = 11;
figure(1)
set(gcf, 'Units', 'centimeters')
set(gcf, 'Position', [8 8 width hight])
set(gcf, 'PaperPositionMode', 'auto')


%% 1st 8 secs, ctrl + enter on this block
clf
box on
hold on
plot(out.tout(1:8000), out.simout1(1:8000),'LineWidth',1);
plot(out.tout(1:8000), out.simout(1:8000),'LineWidth',1);
%title('Drawing head pattern');
xlabel('time [s]') 
ylabel('amplitude [-]') 

legend('reference','output with ILC')
% xlim([-0.7 0.3])
ylim([-30 90])


%% secs 10-14, ctrl + enter on this block to see the figure
clf

hold on
box on
tstart = 14;
tend = 15.5;
plot(out.tout(tstart*1000:tend*1000), out.simout1(tstart*1000:tend*1000),'LineWidth',1);
plot(out.tout(tstart*1000:tend*1000), out.simout(tstart*1000:tend*1000),'LineWidth',1);
%title('Drawing head pattern');
xlabel('time [s]') 
ylabel('amplitude [-]') 

legend('reference','output with ILC')
% xlim([98.5 100])
ylim([-30 90])

%% last 3 seconds of 100 secs, ctrl + enter on this block to see the figure
clf

hold on
box on
tstart = 98.5;
tend = 100;

plot(out.tout(tstart*1000:tend*1000), out.simout1(tstart*1000:tend*1000),'LineWidth',1);
plot(out.tout(tstart*1000:tend*1000), out.simout(tstart*1000:tend*1000),'LineWidth',1);
%title('Drawing head pattern');
xlabel('time [s]') 
ylabel('amplitude [-]') 

legend('reference','output with ILC')
xlim([98.5 100])
ylim([-30 90])


%% detail of last 3 seconds of 100 secs, ctrl + enter on this block to see the figure
clf

hold on
box on
tstart = 98.5;
tend = 100;
plot(out.tout(tstart*1000:tend*1000), out.simout1(tstart*1000:tend*1000),'LineWidth',1);
plot(out.tout(tstart*1000:tend*1000), out.simout(tstart*1000:tend*1000),'LineWidth',1);
%title('Drawing head pattern');
xlabel('time [s]') 
ylabel('amplitude [-]') 

legend('reference','output with ILC')
xlim([99.30 99.45])
ylim([49 67])
%% quadratic evaluation, ctrl + enter on this block to see the figure
clf

hight = 11;
width = 25;
figure(1)
set(gcf, 'Units', 'centimeters')
set(gcf, 'Position', [8 8 width hight])
set(gcf, 'PaperPositionMode', 'auto')

% hold on
box on
% tstart = 98.5;
% tend = 100;
semilogy(out.tout, out.simout2);
grid on
%title('Drawing head pattern');
xlabel('time [s]') 
ylabel('amplitude [-]') 

% legend('reference','output with ILC')
% xlim([99.30 99.45])
% ylim([49 67])
%%