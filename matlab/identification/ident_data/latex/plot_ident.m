%% xx
Ts = 0.002; %500Hz
t = (0:Ts:length(inputset1)/(1/Ts)-Ts);


clf 

hight = 10;
width = 22;
figure(1)
set(gcf, 'Units', 'centimeters')
set(gcf, 'Position', [8 8 width hight])
set(gcf, 'PaperPositionMode', 'auto')

hold on
box on
plot(t(1400:1700),outputset3(1400:1700),'LineWidth',1);
plot(t(1400:1700),out.simout(1402:1702),'LineWidth',1);
%title('Drawing head pattern');
xlabel('t [s]') 
ylabel('I [A]') 

legend('Motor','Model')
xlim([2.8 3.4])
ylim([-.08 .11])

ax = gca;




%%