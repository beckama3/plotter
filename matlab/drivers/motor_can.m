classdef motor_can < realtime.internal.SourceSampleTime ...
        & coder.ExternalDependency ...
        & matlab.system.mixin.Propagates ...
        & matlab.system.mixin.CustomIcon
    % System object block for getting accelerometer & gyro data from Beaglebone Blue.

    
    properties
        % Public, tunable properties.
    end
    
    properties (Nontunable)
        % CAN ID
        can_id = 1;   
    end
    
    properties (Access = private)
        % Pre-computed constants.
    end
    
    methods
        % Constructor
        function obj = Source(varargin)
            % Support name-value pair arguments when constructing the object.
            setProperties(obj,nargin,varargin{:});
        end
    end
    
    
    methods (Access=protected)
        function setupImpl(obj) 
            if isempty(coder.target)
                % Place simulation setup code here                               
                sys = ss(0.9, 1, 0.1, 0);
                
            else
                % Call C-function implementing device initialization
                coder.cinclude('motor_can.h');

                % Call C-function implementing device initialization                
                coder.ceval('can_setup', obj.can_id);

            end
        end
        
        
        function [position,velocity,current] = stepImpl(obj, position_input, velocity_input, Kp, Kd, feed_forward_current)
            
            position_input = single(position_input);
            velocity_input = single(velocity_input);
            Kp = single(Kp);
            Kd = single(Kd);
            feed_forward_current = single(feed_forward_current);
            
            if isempty(coder.target)
                position = single(10);
                velocity = single(10);
                current = single(10);
            else
                % Call C-function implementing device output
                coder.ceval('input_reference', position_input, velocity_input, Kp, Kd, feed_forward_current);
                temp = single(0);
                temp = coder.ceval('return_position');
                position = temp;
                temp = coder.ceval('return_velocity');
                velocity = temp;
                temp = coder.ceval('return_current');
                current = temp;
            end
        end
        
        function releaseImpl(obj) %#ok<MANU>
            if isempty(coder.target)
                % Place simulation termination code here
            else
                % Call C-function implementing device termination
                coder.ceval('can_terminate');
            end
        end
    end
    
    methods (Access=protected)
        %% Define output properties
        function num = getNumInputsImpl(~)
            num = 5;
        end
        
        function flag = isInputSizeLockedImpl(~,~)
            flag = true;
        end
        
        function varargout = isInputFixedSizeImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
            varargout{4} = true;
            varargout{5} = true;
        end
        
        function flag = isInputComplexityLockedImpl(~,~)
            flag = true;
        end
        
        function validateInputsImpl(~, u)
            %if isempty(coder.target)
                % Run input validation only in Simulation
                %validateattributes(u,{'double'},{'scalar'},'','u');
            %end
        end
        
        
        
        
        
        function num = getNumOutputsImpl(~)
            num = 3;
        end
        
        function flag = isOutputSizeLockedImpl(~,~)
            flag = true;
        end
        
        function varargout = isOutputFixedSizeImpl(~,~)
            varargout{1} = true;
            varargout{2} = true;
            varargout{3} = true;
        end
        
        function flag = isOutputComplexityLockedImpl(~,~)
            flag = true;
        end
        
        function varargout = isOutputComplexImpl(~)
            varargout{1} = false;
            varargout{2} = false;
            varargout{3} = false;
        end
        
        function varargout = getOutputSizeImpl(~)
            varargout{1} = [1,1];
            varargout{2} = [1,1];
            varargout{3} = [1,1];
        end
        
        function varargout = getOutputDataTypeImpl(~)
            varargout{1} = "single";
            varargout{2} = "single";
            varargout{3} = "single";
        end
        
        
        
        function icon = getIconImpl(obj)
            icon = {'Motor','Driver'};
            % Define a string as the icon for the System block in Simulink.
            % icon = sprintf('IR LED current: %d mA\n', obj.current);
        end
        
        %pri zavreni nastaveni blocku
        function validatePropertiesImpl(obj)

        end
      
    
    end
    
    methods (Static, Access=protected)
        function simMode = getSimulateUsingImpl(~)
            simMode = 'Interpreted execution';
        end
        
        function isVisible = showSimulateUsingImpl
            isVisible = false;
        end
        
    end
    
    methods (Static)
        function name = getDescriptiveName()
            %name = sprintf('IR LED current: %d mA\n', obj.current);
        end
        
        function b = isSupportedContext(context)
            b = context.isCodeGenTarget('rtw');
        end
        
        function updateBuildInfo(buildInfo, context)
            if context.isCodeGenTarget('rtw')
                % Update buildInfo
                srcDir = fullfile(fileparts(mfilename('fullpath')),'src'); 
                includeDir = fullfile(fileparts(mfilename('fullpath')),'include');
                addIncludePaths(buildInfo,includeDir);
                % Use the following API's to add include files, sources and
                % linker flags
                addIncludeFiles(buildInfo,'motor_can.h',includeDir);
                addIncludeFiles(buildInfo,'communication.h',includeDir);
                addSourceFiles(buildInfo,'motor_can.c',srcDir);
                addSourceFiles(buildInfo,'communication.c',srcDir);
                %addLinkFlags(buildInfo,{'-lSource'});
                %addLinkObjects(buildInfo,'sourcelib.a',srcDir);
                %addCompileFlags(buildInfo,{'-D_DEBUG=1'});
                %addDefines(buildInfo,'MY_DEFINE_1')
            end
        end
    end
end
