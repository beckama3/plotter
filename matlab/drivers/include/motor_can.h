#ifndef MTR_CAN
#define MTR_CAN

#include <signal.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

#include <pthread.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>

#include <string.h>
#include <sys/ioctl.h>
#include <sys/timerfd.h>


void print_debug(const char *fmt);
int can_setup(int id);
int send_can_frame(__u8 message[]);
int input_reference(float position, float velocity, float Kp, float Kd, float feed_forward_current);
float return_position();
float return_velocity();
float return_current();
int can_terminate();



#ifdef PRINTDEBUG
    #define DEBUG(x) print_debug(x)
	#define DEBUG_MESS(x) print_debug_message(x)
#else
    #define DEBUG(x) 
	#define DEBUG_MESS(x)
#endif



#ifdef __cplusplus
extern "C" {
#endif
      
#ifdef __cplusplus
}
#endif
 
#endif