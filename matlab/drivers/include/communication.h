#ifndef COMM_LIMITS_H
#define COMM_LIMITS_H

#include <stdint.h>
#include <math.h>
#include <stdbool.h>

#define P_LIM 12.5f
#define V_LIM 270.0f
#define KP_MAX 50.0f
#define KD_MAX 1.0f
#define T_LIM 3.0f

#define GYRO_LIM   34.9065850399f //  2000 dps
#define ACCEL_LIM  156.96f        //  16 g
#define PITCH_LIM  1.57079632679f //  pi/2
#define ROLL_LIM   3.14159265359f //  pi
#define YAW_MAX    6.28318530718f //  2*pi

#define UART_MAX_MESSAGE_SIZE 200

#define MOTOR_COMMAND               0x01
#define CHANGE_STATE                0x02
#define READ_MOTOR_MEASUREMENTS     0x03
#define READ_MOTOR_MEASUREMENTS_ALL 0x04
#define READ_IMU_MEASUREMENTS_EULER 0x05
#define READ_MEASUREMENTS_ALL       0x06
#define ZERO_POSITION               0x09
#define REFERENCES_COMMAND          0x10
#define TRAJECTORY_DATA             0x11
#define MOTOR_COMMAND_ALL           0x12

// Status byte:
// 0-3 ; fail states of motors
// 5 ; sttus in air
// 6-7 ; leg in contac bits
#define STATUS_MOTOR_MASK           0x0F
#define STATUS_IN_AIR               5
#define STATUS_LEG_CONTACT_L        6 // also know as wheelInContact :/ 
#define STATUS_LEG_CONTACT_R        7

struct trajElement {
    uint8_t data[8];
};

#define TRAJECTORY_MAX_LENGTH      5000 // Maximum number of trajectory elements in a trajectry

inline float fmaxf(float x, float y){
    /// Returns maximum of x, y ///
    return (((x)>(y))?(x):(y));
}
 
inline float fminf(float x, float y){
    /// Returns minimum of x, y ///
    return (((x)<(y))?(x):(y));
}

inline float my_fmod(float a, float b) {
    return (a - b * floor(a / b));
}



int float_to_uint16_symmetric(float x, const float x_max)
{
    float span = 2 * x_max;
    float quantum = span/((1<<16)-1);

    float offset = 0;
    offset = -x_max - quantum/2;    
    
    return (int) ((x-offset)/quantum + 0.5);
}

int float_to_uint12_symmetric(float x, const float x_max)
{
    float span = 2 * x_max;
    float quantum = span/((1<<12)-1);

    float offset = 0;
    offset = -x_max - quantum/2;    
    
    return (int) ((x-offset)/quantum + 0.5);
}

int float_to_uint12_positive(float x, const float x_max)
{
    float span = x_max;
    float quantum = span/((1<<12)-1);

    float offset = 0;
        
    return (int) ((x-offset)/quantum + 0.5);
}

float uint_to_float16_symmetric(int x, const float x_max)
{
    float span = 2 * x_max;
    float quantum = span/((1<<16)-1);
    
    float offset = 0;
    
    offset = -x_max - quantum/2;    
    
    return ((float)x)*quantum + offset;
}

float uint_to_float12_symmetric(int x, const float x_max)
{
    float span = 2 * x_max;
    float quantum = span/((1<<12)-1);
    
    float offset = 0;
    
    offset = -x_max - quantum/2;    
    
    return ((float)x)*quantum + offset;
}

float uint_to_float12_positive(int x, const float x_max)
{
    float span = x_max;
    float quantum = span/((1<<12)-1);
    
    float offset = 0;
        
    return ((float)x)*quantum + offset;
}

void pack_motor_reference(uint8_t* compact_data, float p_des, float v_des, float kp, float kd, float t_ff);
void unpack_motor_reference(const uint8_t* compact_data, float* p_des, float* v_des, float* kp, float* kd, float* t_ff);

void pack_motor_measurement(uint8_t* compact_data, float p_meas, float v_meas, float t_meas);
void unpack_motor_measurement(const uint8_t* compact_data, float* p_meas, float* v_meas, float* t_meas);

#endif
