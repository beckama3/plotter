#include "communication.h"


/// CAN Command Packet Structure ///
/// 16 bit position command, between -4*pi and 4*pi				
/// 12 bit velocity command, between -30 and + 30 rad/s
/// 12 bit kp, between 0 and 500 N-m/rad
/// 12 bit kd, between 0 and 100 N-m*s/rad
/// 12 bit feed forward torque, between -18 and 18 N-m

// #define P_LIM 12.5f
// #define V_LIM 270.0f
// #define KP_MAX 50.0f
// #define KD_MAX 0.5f
// #define T_LIM 3.0f

/// CAN Packet is 8 8-bit words
/// Formatted as follows.  For each quantity, bit 0 is LSB
/// 0: [position[15-8]]
/// 1: [position[7-0]] 
/// 2: [velocity[11-4]]
/// 3: [velocity[3-0], kp[11-8]]
/// 4: [kp[7-0]]
/// 5: [kd[11-4]]
/// 6: [kd[3-0], torque[11-8]]
/// 7: [torque[7-0]]



void pack_motor_reference(uint8_t* compact_data, float p_des, float v_des, float kp, float kd, float t_ff) {
    // limit the data to be within the communcation limits //
    p_des = fminf(fmaxf(-P_LIM, p_des), P_LIM);
    v_des = fminf(fmaxf(-V_LIM, v_des), V_LIM);
    kp = fminf(fmaxf(0.0, kp), KP_MAX);
    kd = fminf(fmaxf(0.0, kd), KD_MAX);
    t_ff = fminf(fmaxf(-T_LIM, t_ff), T_LIM);
    
	
	/// convert floats to unsigned ints ///
    int p_int  = float_to_uint16_symmetric(p_des, P_LIM);            
    int v_int  = float_to_uint12_symmetric(v_des, V_LIM);
    int kp_int = float_to_uint12_positive(kp, KP_MAX);
    int kd_int = float_to_uint12_positive(kd, KD_MAX);
    int t_int  = float_to_uint12_symmetric(t_ff, T_LIM);

    /// pack ints into the can buffer ///
    compact_data[0] = p_int>>8;                                       
    compact_data[1] = p_int&0xFF;
    compact_data[2] = v_int>>4;
    compact_data[3] = ((v_int&0xF)<<4)|(kp_int>>8);
    compact_data[4] = kp_int&0xFF;
    compact_data[5] = kd_int>>4;
    compact_data[6] = ((kd_int&0xF)<<4)|(t_int>>8);
    compact_data[7] = t_int&0xff;
}

void unpack_motor_reference(const uint8_t* compact_data, float* p_des, float* v_des, float* kp, float* kd, float* t_ff) {
    uint16_t p_int  = (compact_data[0]<<8) | compact_data[1];
    uint16_t v_int  = (compact_data[2]<<4) | (compact_data[3]>>4);
    uint16_t kp_int = (compact_data[3] & 0xF)<<8 | compact_data[4];
    uint16_t kd_int = (compact_data[5]<<4) | (compact_data[6]>>4);
    uint16_t t_int  = (compact_data[6] & 0xF)<<8 | compact_data[7];

    *p_des = uint_to_float16_symmetric(p_int, P_LIM);
    *v_des = uint_to_float12_symmetric(v_int, V_LIM);
    *kp    = uint_to_float12_positive(kp_int, KP_MAX);
    *kd    = uint_to_float12_positive(kd_int, KD_MAX);
    *t_ff  = uint_to_float12_symmetric(t_int, T_LIM);
}

/// compact_data[0]: [position[15-8]]
/// compact_data[0]: [position[7-0]] 
/// compact_data[0]: [velocity[11-4]]
/// compact_data[0]: [velocity[3-0], torque[11-8]]
/// compact_data[0]: [torque[7-0]]
void pack_motor_measurement(uint8_t* compact_data, float p_meas, float v_meas, float t_meas) {
    int p_int = float_to_uint16_symmetric(p_meas, P_LIM);
    int v_int = float_to_uint12_symmetric(v_meas, V_LIM);
    int t_int = float_to_uint12_symmetric(t_meas, T_LIM);

    compact_data[0] = p_int>>8;
    compact_data[1] = p_int&0xFF;
    compact_data[2] = v_int>>4;
    compact_data[3] = ((v_int&0xF)<<4) + (t_int>>8);
    compact_data[4] = t_int&0xFF;
}

void unpack_motor_measurement(const uint8_t* compact_data, float* p_meas, float* v_meas, float* t_meas) {
    /// unpack ints from can buffer ///
    uint16_t p_int = (compact_data[1]<<8)|compact_data[2];
    uint16_t v_int = (compact_data[3]<<4)|(compact_data[4]>>4);
    uint16_t i_int = ((compact_data[4]&0xF)<<8)|compact_data[5];

    /// convert ints to floats ///
    *p_meas = uint_to_float16_symmetric(p_int, P_LIM);
    *v_meas = uint_to_float12_symmetric(v_int, V_LIM);
    *t_meas = uint_to_float12_symmetric(i_int, T_LIM);
}
