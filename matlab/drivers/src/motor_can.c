/*********** Includes *************/
//#define PRINTDEBUG

#include "motor_can.h"

void pack_motor_reference(uint8_t* compact_data, float p_des, float v_des, float kp, float kd, float t_ff);
void unpack_motor_reference(const uint8_t* compact_data, float* p_des, float* v_des, float* kp, float* kd, float* t_ff);
void pack_motor_measurement(uint8_t* compact_data, float p_meas, float v_meas, float t_meas);
void unpack_motor_measurement(const uint8_t* compact_data, float* p_meas, float* v_meas, float* t_meas);



FILE *fp;

static int can_fd;
int can_id;

float p_meas, v_meas, t_meas;
float p_meas_prev = 0;
int overflow = 0;

// pthread_t tid;

struct can_frame recieved_frame;

void print_debug(const char *fmt){
	fprintf(fp, fmt);
	fprintf(fp, "\n");
}

void print_debug_message(uint8_t message[]){
	DEBUG("Message:");
	fprintf(fp, "%u", message[0]);
	fprintf(fp, "%u", message[1]);
	fprintf(fp, "%u", message[2]);
	fprintf(fp, "%u", message[3]);
	fprintf(fp, "%u", message[4]);
	fprintf(fp, "%u", message[5]);
	fprintf(fp, "%u", message[6]);
	fprintf(fp, "%u", message[7]);
	fprintf(fp, "%u", message[8]);
	fprintf(fp, "\n");
}

int send_can_frame(uint8_t message[]) {
	
	struct can_frame fra;
    uint8_t size = 0;
    fra.can_id = can_id;
    fra.can_dlc = 8;

	
	DEBUG("can_frame message:");
	int i;
	for(i = 0; i < 8; ++i){
		fra.data[i] = message[i]; 
	}

    size = send(can_fd, &fra, sizeof(struct can_frame), MSG_DONTWAIT);
    if(size != sizeof(struct can_frame))
    {
        DEBUG("Problem writing frame\nIs your CAN interface 'up'");
        return 0;
    }
    else
    {
        DEBUG("Sending success.");
        return 0;
    }
    return 0;	
}


int enter_motor_mode()
{
	DEBUG("Motor mode...");
	
    uint8_t message[8] = {255,255,255,255,255,255,255,252};
	
	DEBUG("Entering motor mode...");
	
	return send_can_frame(message);
}

int disable_motor_mode()
{	
	uint8_t message[8] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFD};
	
	return send_can_frame(message);
}


int can_setup(int id){
	
		
	#ifdef PRINTDEBUG
		fp = fopen("error_output.txt", "w+");
	#endif
	
		
	can_id = id;	
	struct ifreq ifr;  /* struct to configure socket file descriptor */
	struct sockaddr_can addr;  /* address family number AF_CAN. : CAN network interface index. */
	
	
    /* Create a socket */
    can_fd = socket(AF_CAN, SOCK_RAW, CAN_RAW);
    if (can_fd < 0)
    {
        DEBUG("Cannot create socket, quitting");
        return 1;
    }
    strcpy(ifr.ifr_name, "can0");

    /* Associate socket with interface (can0) */
    if(ioctl(can_fd, SIOCGIFINDEX, &ifr) < 0)
    {
        DEBUG("Cannot link socket with interface - does the interface exist?");
        return 1;
    }

    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

	
    if (bind(can_fd, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        DEBUG("Unable to bind socket, quitting");
        return 1;
    }
	
/* 	//thread for listening to can
	int err;
	err = pthread_create(&tid, NULL, &doSomeThing, NULL);
	
    if (err != 0){
        DEBUG("\ncan't create thread :[%s]", strerror(err));
	}
    else{
        DEBUG("\n Thread created successfully\n");
	} */
	
	
	if(enter_motor_mode()){
        DEBUG("Unable to enable motor mode, quitting");
        return 1;		
	}else{
		DEBUG("Motor mode enabled");
	}
	
	
	DEBUG("End of the setup.");

	
	return 0;	
}



int input_reference(float position, float velocity, float Kp, float Kd, float feed_forward_current){
	
	int nbytes;
	nbytes = read(can_fd, &recieved_frame, sizeof(struct can_frame));
	DEBUG("can frame read");	
	//fprintf(fp, "data length: %i", recieved_frame.can_dlc);
	unpack_motor_measurement(recieved_frame.data, &p_meas, &v_meas, &t_meas);
	//fprintf(fp, "%i", nbytes);
	
	uint8_t message[8];
	//pack_motor_reference(message, position, velocity, Kp, Kd, feed_forward_current);
	
	
	
	pack_motor_reference(message, position, velocity, Kp, Kd, feed_forward_current);
	
	DEBUG_MESS(message);	
	
	send_can_frame(message);
		
	
	DEBUG("references sent");
	return 0;
}


float return_position(){
	float check_overflow = p_meas_prev - p_meas;
	
	if(check_overflow > 20){
		overflow++;
	}else if(check_overflow < -20){
		overflow--;
	}	
	p_meas_prev = p_meas;
	
	float ret = p_meas + (overflow * 25);	
	return ret;
}                       
float return_velocity(){
    return v_meas;
}       
float return_current(){
    return t_meas;
}     


int can_terminate(){
	
	if(disable_motor_mode())
	{
		DEBUG("Failed to turn the motor off!");		
	}else{
		DEBUG("Motor turned off");
	}
	
	if(can_fd > 0)
    {
        close(can_fd);
		DEBUG("closing can...");
    }
		
	
	#ifdef PRINTDEBUG
		DEBUG("Closing file descriptor of this file...");
		fclose(fp);
	#endif
    
    return 0;
} 