function p = pub_fig(x,y,line_width,line_style,line_color,marker,marker_size, marker_edge_color,marker_face_color)


axis_label_font_size = 12;
title_font_size = 16;


p = line(x,y);

set(p,'linewidth',line_width,'linestyle',line_style,'color',line_color);
set(p,'marker',marker,'markeredgecolor',marker_edge_color,'markerfacecolor',marker_face_color,'markersize',marker_size);
xt = input('enter the xaxis label','s');
yt = input('enter the yaxis label','s');
tt = input('enter the title','s');
le = input('enter the legend','s');

xl = xlabel(xt);yl = ylabel(yt); tl = title(tt);
leg = legend(p,le);
set(xl, 'fontweight','bold','fontsize',axis_label_font_size); set(yl,'fontweight','bold','fontsize',axis_label_font_size);
set(tl,'fontweight','bold','fontsize',title_font_size)