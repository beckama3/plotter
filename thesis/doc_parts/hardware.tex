% hardware chapter
\chapter{Hardware}
\label{chapter:hardware}
\section{Plotter description}
First I have to state, that I have not participated in the physical construction of the plotter by any way. However since our plotter has not been described anywhere before, let me do a brief overview of the construction and mechanics of it.

Traditional printers use Cartesian mechanics, where the printing head is moved by one motor in $x$ and other in the $y$ coordinate. That has one basic disadvantage - at least one motor is moving, which increases the inertia of moving parts. Higher inertia means lower possible accelerations, which is not desirable. If there is a requirement for the highest speed possible, this construction cannot satisfy our needs.

The basic idea of the plotter is to have motors stationary, mounted to the non moving base of the printer. H-bot mechanical system allows just that, since forces are transferred with a system of belt and pulleys to a gantry and printing head. On the gantry, there are four pulleys and it can only move up and down (relative to the plotter), while the printing head moves only left and right (relative to the gantry)\footnote{Nice animation of the H-bot can be found i.e. here \url{https://www.youtube.com/watch?v=IkM2K7CsiHo}}. This setup is not only faster due to the reasons mentioned above, but also more accurate because of no accumulated error\cite{Sollmann:2007}. For completeness and to avoid any confusion, I will also mention the coreXY system, which we don not use. CoreXY uses a little different belt design, but the key idea remains the same.

Another requirement is to use BLDC motors. Note that in most scenarios, printers or plotters use stepper motors, because they are precise and easier to control. However, we are not so much trying to develop ``the best plotter'', but rather a development platform for control algorithms - in which BLDC motors fit better thanks to their greater dynamics.

\begin{figure}[H]
    \centering
    \includegraphics[width=8cm]{figs/h-bot}
    \caption{Illustration of the H-bot plotter mechanism}
\end{figure}


\section{Electronics}
\subsection{Motor driver}
Although I was free to design basically anything, it is always better to reuse existing hardware, if it satisfies my needs. Luckily, our department already had experience with a SK8O robot \footnote{\url{https://control.fel.cvut.cz/nasi-doktorandi-postavili-robota-balancujiciho-na-dvou-nohach-budou-ho-ucit-skakat-do-schodu}}. This robot uses motor driver that has been originally developed by Ben Katz for a Mini Cheetah on MIT \cite{Sollmann:2007} \cite{Kim:2019}. It is a 40 kHz 3 phase controller for BLDC motors. Currently, there exists a customized version of the board designed on department of control engineering and a port of the original driver is being developed by Bc. Petr Brož\footnote{\url{https://github.com/ptrbroz/motorcontrol_stm32g474re}}. 

However, I was provided an older version of the driver, the same that is used on SK8O. The main difference is, the newer version uses CAN FD (Controller Area Network Flexible Data-Rate) instead of the older, traditional CAN (Controller Area Network). That means some additional work needs to be done to adapt adequately to these changes, if the final plotter will use the newer driver board. I am sure that it will not be much trouble because it is not a revolutionary change.

\subsection{Motor}
The Driver can control a wide range of BLDC motors. However, it is assumed that eX8108 105KV brushless DC motors will be used on the final plotter. It is the same model as on original MIT's Mini Cheetah and on SK8O. The maximum power output is 750 W and on the final platform there will be used two motors. For the development I had access to one driver with this motor.

\begin{figure}[h]
    \centering
    \includegraphics[width=7cm]{figs/eX8108-105KV}
    \caption{eX8108-105KV brushless DC motor}
\end{figure}

%diagram?

\subsection{Control computer}

As the main brain of the plotter, I chose popular single board computer BeagleBone Blue. It has integrated CANbus receiver and transceiver, which means we can communicate with the motor driver with no other components needed. Note that some development boars have CAN support as well, but they are not able to transceive CAN messages without additional chip (i.e. BeagleBone Black). 

Another big advantage of BeagleBone Blue is integrated Matlab support in form of \textit{Simulink package for BeagleBone Blue}. This package implements many of basic functions of the board, including basic things such as GPIO LED blinking, integrated H-bridge motor control, and an encoder. One can easily program a simple scheme using Simulink and deploy it to the hardware, or use it in external mode to enable live feed of data to the Simulink. However, for the start our development we need only one feature, CAN bus support, which this package does not natively implement. Therefore I had to develop a driver for this purpose. For a clear distinguishing between the physical driver board by Ben Katz, high-level control software and this low level Simulink BeagleBone support with CAN packet decoding, I will call it firmware.


\section{Firmware}
As mentioned earlier, I will use Matlab and Simulink for basically all control algorithms that need to be developed. Therefore some interface between the BeagleBone and Simulink needs to be set up. For this purpose, I used Matlab System Object, which is a powerful specialized object, which can among others call external C functions from custom written C code. This code is compiled using Matlab Coder and then deployed on our BeagleBone. In the C language, I will write my CAN bus communication and all the decoding arithmetics of the packets.

Behaviour and appearance of the system object block is controlled by the \verb|.m| file with the corresponding name, in my case \verb|motor_can.m|. In this file it is defined input and output port number and types, parameters, and C function calling logic. The design was done with an emphasis on the reusability of the driver, even or other projects or applications. 


I split the C code into two files, namely \verb|motor_can.c| and \verb|communication.c|. Header files of corresponding names are also made. My code uses SocketCAN driver, which provides an interface similar to Internet Protocol via the sockets. For the communication we need to do several steps, similar to TCP/IP communication - first the socket needs to be initialized and created, then the application will bind this socket to an existing can interface. After that, we can use read from and write to the socket.

\subsection{motor\_can.c functions}
 Although the purpose of all the functions is well documented in the code, for clarity let me sum it up here. I will omit the input and output arguments, as it will only make the text excessively long and therefore less clear. For more details, the reader can always check the code itself.
  
Function \verb|print_debug()| is for debugging purposes only. The debug messages can be activated by \verb|#define PRINTDEBUG| statement at the top of the file, which then ensures the \verb|print_debug| function is called every time \verb|DEBUG(``debug message'')| is called. 

\verb|can_setup()| first initializes the error output file, if the user wishes to (via the procedure mentioned above). The main function however is to open and configure the can socket and associate it to the defined can interface. The last functionality calls enter\_motor\_mode function, so the communication with specified can ID can begin.

int \verb|enter_motor_mode()| sends a special command 0xFFFFFFFC for the motor to enter its operating ``motor'' mode.

Similarly \verb|disable_motor_mode()| deactivates the motor by sending 0xFFFFFFFD command to it. 

All the can messages are sent via the \verb|send_can_frame()| function. 

Function \verb|input_reference()| takes all the inputs (position, velocity, Kp, Kd, feed forward current) and sends them the motor. It also receives the answer and unpacks it using \verb|unpack_motor_measurement()| to the global variables of respective names.

Functions \verb|return_current|, \verb|return_velocity|, and \verb|return_position| are simple getters of respective values, with an exception of the latter one. The \verb|return_position| also check for potentinal discontinuity, and patches it. The discontinuity appears when a motor encoder crosses its origin position.

Finally, the \verb|can_terminate| first disables the motor mode calling \verb|disable_motor_mode| function. It also closes the socket and connection, and the file descriptor of debug file if it was created in the first place.

\subsection{communication.c functions}
Similarly to the main c file \verb|motor_can.c|, I will briefly mention the functions of a communication file. As the name suggests, it has a purpose of packing and unpacking the messages of can communication. Note that template is available in \ref{doplnit ben katz}, but it is written in C\texttt{++}. Since I had no success making C\texttt{++} compilation work with Matlab Coder and because of some custom requirements, I had to write it basically from a scratch.

There are two main executive functions in the communication. First being the \verb|pack_motor_reference|, which takes float numbers from the matlab and translates them into a can message. Secondly, \verb|unpack_motor_measurement| translates the answer from the motor and translates it into a floats, which Matlab can work with. Both the functions call the lowest level bit operators from header files. 

These also have their counterparts,  \verb|unpack_motor_reference|, and \verb|pack_motor_measurement|, which exist only for help with development purposes, because the hardware was not available all the time due to covid situation.





\subsection{Simulink driver function \& verification}
Unfortunaly I do not have any data of simple experiments, that could show driver functionality. Right now the hardware is not available anymore, but I have one video of the motor following rectangular waveform position reference\footnote{\url{https://youtu.be/J_v39mfhuxk}}. 


Let me explain the basic functionality. The user can set five inputs, the motor driver receives all of them and combine all of them together to get one command for the motor. The values being position and velocity inputs, which are set as a reference in a control loop with PD regulator with customizable coefficients $Kp$ and $Kd$. Tested values of the constants with no load and decent behaviour are i.e. $Kp = 3$ and $Kd = 0.3$. If we do not want to use any position or velocity reference, we have to set these values to zeros (the zero position and velocity are not enough, it would cause the motor to move to zero). On the output, the motor replies with actual position, velocity, and current flow. All values are a float type (single precision).

\begin{figure}[H]
    \centering
    \includegraphics[width=6cm]{figs/motor_driver}
    \caption{Simulink block of motor driver}
\end{figure}


\subsection{Motor identification}

Since the motor driver returns the information about an actual current floating it, and I can also set a reference current, I performed an experiment in purpose to obtain a transfer function of these two. I've focused on the current because the assumption is that I am going to use it in my ILC control experiments to have more control over the motor, rather than using integrated reference/velocity controller.

For the identification purpose, I first set all commands to zero, varying only the feed\_forward\_current input. I choose PRBS - pseudo random binary sequence with defined bandwidth and sampling frequency, generated using \verb|idinput| function of Matlab. I generated 3 sets of 120 second long data, two for model fit and one for verification. This signal is then fed to the motor, which should ideally excite all its modes. The experiment of course was performed over the CAN communication, which had a frequency of \SI{1}{\kilo\hertz}. That with the noisy data has emerged as a likely limiting factor, as almost no dynamics can be observed and extracted. Using System Identification Toolbox I obtained a discrete 4th order model  with an 80\% fit to the testing data. Equation of the transfer (rounded to 4 decimal places) is on eq. \ref{eq:current_transfer} and comparison to the data (only a fraction of the data) can be seen in the fig. \ref{fig:current_transfer_comparison}.

\begin{equation}\label{eq:current_transfer}
H(z) = \frac{-7.90e-5 z^2+0.0348z-0.0365}{z^4-1.341z^3+0.3868z^2-0.0278z-0.0021}
\end{equation}


\begin{figure}[H]
    \centering
    \includegraphics[width=14cm]{figs/current}
    \caption{Model and motor response comparison}
    \label{fig:current_transfer_comparison}
\end{figure}





