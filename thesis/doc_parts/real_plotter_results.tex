\chapter{Experiments with the model}
\label{chapter:experiments}

\section{Model overview}
As mentioned earlier, due to hardware manufacturing delays, I had no access to the physical plotter. For the control algorithms development, I had to fully rely on simulation. Therefore I needed an accurate model of the physical plant, which I can rely on. Since the development of such a model is out of the scope of this thesis and could be quite complex and time consuming, my fellow colleague Adam Kollarčík came with a helping hand. He provided me a Simulink model developed with the use of bond graphs techniques. I want to declare, that I have \iffalse \bold{not}\fi contributed to the model development in any way. My only task was to understand and verify the model or adjust the parameters to comply with my needs. 

The model has two inputs T1 and T2, which represents torque input to the motor 1 and 2 in newton meters. On the output side, we can observe the state of both the motors and the drawing head. Motors have $\omega_{1,2}$, head $v_{x,y}$. With their integrated counterparts $\theta_{1,2}$ and $position_{x,y}$ it makes a total of 8 outputs. Make note that in contrast, the currently proposed plotter has no measurement of the drawing head position. We can only observe the result - drawn shape - and use data from the motors on the ``other side'' of the belt. 

In the model, there are parameters for all the friction, weights and compliance of all the individual belt sections. These parameters were estimated on the basis of this master thesis, that deals with the same problem \cite{Sollmann:2007}.


\section{Model verification}
\subsection{Kinematics}
To verify the model, first I need to determine the ideal plotter kinematics. For that I assume all the pulleys to be the same unit radius. Then, using sign notation and quantities labels as in the fig. \ref{fig:kinematics} I can write
\begin{equation}\label{eq:h-bot_kinematics}
x = \frac{1}{2}(\theta_1 + \theta_2)
\end{equation}
\begin{equation}\label{eq:h-bot_kinematics2}
y = \frac{1}{2}(\theta_1 - \theta_2)
\end{equation}

\begin{figure}[h]
    \centering
    \includegraphics[width=8cm]{figs/h-bot3}
    \caption{H-bot coordinate system}
    \label{fig:kinematics}
\end{figure}

\subsection{Sawtooth experiment}
To get used to the mechanics of the H-bot kinematics and also to verify the model, I performed several simple experiments. First of all, I had to develop some kind of a simple feedback control, so we can track a reference trajectory. For this purpose, I tuned a simple PID controller by intuition. For the first try, I tried one independent control loop per each motor from motor angle $\theta$. I set $K_p = 3$, $K_i = 5$ and $K_d = 0.3$. With the feedback set up, let's try to fix the motor (set the reference to zero) and move only with the other one to draw a diagonal.



\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{figs/1st_experiment_reference}
  \caption{Reference}
  \label{fig:diagonal1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{figs/1st_experiment_drawing}
  \caption{Drawing}
  \label{fig:diagonal2}
\end{subfigure}
\caption{Diagonal drawing}
\label{fig:diagonal}
\end{figure}

And indeed, the model behaviour seems to be reasonable. Here we can notice a slight cross-coupling between the motors, which will later be a challenge to deal with. This coupling originates from friction between the belt and pulleys on the gantry. Its magnitude is tunable in \verb|parameters.m|.

\subsection{Square experiment}
Now let's try to draw a square. I chose the simple square shape as a general reference drawing path, due to its basic nature and also because its sharp corners and the sequent control challenge. 

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{figs/square_reference}
  \caption{Reference}
  \label{fig:square1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{figs/square_drawing}
  \caption{Drawing}
  \label{fig:square2}
\end{subfigure}
\caption{Square drawing}
\label{fig:square}
\end{figure}

We can see the head to draw a decent square, however with some overshoots. The overshoots seem to be much higher in the $y$ direction than in $x$. And it makes sense since in parameters the weight in $y$ direction is approximately 10 times higher than in $x$ direction. That models the additional weight of the gantry and subsequent inertia causes a bigger overshoot.





\iffalse
\begin{figure}[!tbp]
  \centering
  \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{figs/1st_experiment_reference}
    \caption{Reference}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.49\textwidth}
    \includegraphics[width=\textwidth]{figs/1st_experiment_drawing}
    \caption{Drawing}
  \end{minipage}
\end{figure}
\fi

\section{Control requirements}
Before proceeding further, let me state some theoretical control requirements. Looking at the eX8108-105KV datasheet, max peak torque \SI{3}{\newton\meter} seems like a reasonable requirement. The motor can safely deliver that when the speed is lower than \SI{30}{\radian\per\second}. Control loop \SI{1}{\kilo\hertz} is a verified value from previous projects and the BeagleBone can handle it with no problem, at least for one motor that I could test. Measured quantities are motor positions, angular velocities, and actual current. 

However, that is the theory. Looking at the control action of conservative PID square drawing (fig. \ref{fig:square}), the peak value of control signal $u_j$ is over \SI{10}{\newton\meter}, with the mean being over \SI{5}{\newton\meter}. I briefly looked at it and ask my colleague with no clear conclusion. The reference is a small simple square drawn in $t = 1 s$, which looks like a reasonable requirement every plotter should easily draw. The solution I decided for is to simply ignore this and do not look for the root of the problem, since the model parameters are estimated anyway and based on previous thesis of someone else. When we have the actual hardware, we can fine tune and verify the model and work with it. 

Since on my model, it looks the control action is one order of magnitude higher, I choose my limit to be 10 times higher of $T_{max} = 30$.

\section{ILC experiments on the plotter}
\subsection{Naive implementation}
The next step I tried is to implement my ILC block to improve the square drawing. The reference remains the same, see (fig. \ref{fig:square1}). Parameters of feedback controller and ILC are the same as in ILC verification experiments \ref{section:ILC_verification} (in fact they were tuned for this experiment in the first place). In this experiment, I approached the problem in SISO way, i.e. two independent control loops from $\theta$ to model input.

\begin{figure}[h]
    \centering
    \includegraphics[width=12cm]{figs/ilc_plotter_siso}
    \caption{ILC implementation attempt}
    \label{fig:siso_plotter}
\end{figure}


Parameters summary: feedback controller $C$ with coefficients $K_p = 3$, $K_d = 0.1$. The learning function of the ILC has $ILC_{kp} = 0.1$ and $ILC_{kd} = 0$. For a filter $Q$ i used the conservative advanced one, with $F_{pass} = \SI{0}{\hertz}$, $F_{stop} = \SI{20}{\hertz}$, attenuation magnitude of 30 dB. The result can be observed in the fig. \ref{fig:ilc_fail}.

\begin{figure}[h]
    \centering
    \includegraphics[width=12cm]{figs/ilc_fail}
    \caption{ILC implementation attempt}
    \label{fig:ilc_fail}
\end{figure}

The drawing cannot be described as anything other than a total failure. In the figure there are only 6 iterations and the drawing already diverged to the point that it is not clear what the reference is. The control action diverges as well. Clearly, the crosscoupling in the model is strong enough to cause a fatal instability in our control loop. While trying to solve it, I tried both lower learning coefficients or more aggressive low pass filter $Q$. It does not solve the problem, it could only make it appear a little later.

\subsection{MIMO approach}
Since single input, single output control strategy is insufficient for this scenario, the next step is to try our luck with a MIMO controller design. There is an extensive theory and tons of methods in this area of control, however, after consultation with my supervisor, I decided to focus on $\mathcal{H_{\infty}}$ family of methods.

$\mathcal{H_{\infty}}$ methods are used for obtaining the controller, which is in a mathematical sense optimal and can guarantee performance. $\mathcal{H_{\infty}}$ synthesis is the process, that calculates the controller that minimizes the gain between external input to error signal while using the least amount of actuator energy. The trick is to design the control problem, so the existing solver understands it and can be used. For this purpose, I designed a generalized plant, which contains a model of our plotter, the measured signal we have access to, inputs we have control over, and other signals we want to consider in this problem. All these signals are weighted, and the weights can be frequency dependent (i.e. can be treated with a transfer function). With regard to potential application implementation and because of the ILC augmentation simplicity, I decided to force a controller structure. In my case, I simply added two PID controllers to create a MIMO 2x2 controller (see fig. \ref{fig:hinf_setup1}). Then I added performance weights to the inputs of the plant $Wu1, Wu2$, to output of the plant $Wt1, Wt2$ and to the control error $Wp1, Wp2$. The labels and procedure was done in compliance with \cite{Skogestad:2005}.

\begin{figure}[h]
    \centering
    \includegraphics[width=13cm]{figs/plotter_mimo}
    \caption{$\mathcal{H_{\infty}}$ fixed structure mimo control setup}
    \label{fig:hinf_setup1}
\end{figure}


\begin{figure}[H]
  \makebox[\textwidth][c]{\includegraphics[width=1.2\textwidth]{figs/generalized}}
  \caption{Generalized plant}
  \label{fig:generalized}
\end{figure}


That can be done using \verb|hinfstruct| command in Matlab, which is searching for the optimization minimum by varying only the defined parameters. In our case, the parameters were $k_p, k_i, k_d$ and derivative filter parameter in PID controllers $C1$, $C12$, $C21$ and $C2$ (see eq. \ref{eq:tunable_PID}).
 The full generalized  plant with $\mathcal{H_{\infty}}$ synthesis using \verb|hinfstruct| command is implemented in \verb|fixed_stucture.m|. However, for my next experiments, I used simplified variant of the plant, using only $Wt$ and $Wu$ signals, implemented in \verb|fixed_structure_simple| file.



%

\begin{equation}\label{eq:tunable_PID}
C = K_p + K_i\frac{1}{s}+ K_d\frac{s}{T_fs+1}
\end{equation}


Before I could proceed, I needed to obtain a linear model of the plotter. That was done by trimming the model around zero and using Model Linearizer app in the Simulink. One modification I have done is to delete the integrated outputs ($\theta_{1,2}$ and $pos_{x,y}$) from the model before linearization, so the solver does not have a hard time with them. The results are stored in the \verb|linearization| folder.

Finally, let's see what result this controller can achieve. Reference is the same as usual, but this time derived $\omega$ instead of $\theta$, because we deleted these integrated outputs. The drawing from this controller on \ref{fig:Hinf_plotter_square}.


\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{figs/Hinf_plotter_square}
  \caption{Drawing}
  \label{fig:square1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{figs/Hinf_plotter_square_detail}
  \caption{Detail}
  \label{fig:square2}
\end{subfigure}
\caption{Square drawing with $\mathcal{H_{\infty}}$ optimal controller}
\label{fig:Hinf_plotter_square}
\end{figure}

It looks great, at a glance almost perfect. In the detail of the corner, we can still see an imperfection, which could improve. On the peaks, the control action of this controller overshoots the specified limit, however I included a saturation block, so stays within the specified limit, see fig. \ref{fig:hinf_control_action}.
\begin{figure}[h]
    \centering
    \includegraphics[width=15cm]{figs/h_inf_control_action}
    \caption{Control action of $\mathcal{H_{\infty}}$ sythetized controller}
    \label{fig:hinf_control_action}
\end{figure}


Let's try to augment this system with an ILC and see, it if gets better. I chose to try the ILC only on $C1$ and $C2$, see fig. \ref{fig:hinf_ilc_setup}.


\begin{figure}[h]
    \centering
    \includegraphics[width=13cm]{figs/hinf_ilc_setup}
    \caption{ILC mimo control setup}
    \label{fig:hinf_ilc_setup}
\end{figure}



%\ref{fig:ILC_plotter_square}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{figs/ILC_square_detail}
  \caption{Drawing detail}
  \label{fig:ILC_detail1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{figs/ILC_square_detail_last}
  \caption{Drawing detail; $25^{th}$ iteration}
  \label{fig:ILC_detail2}
\end{subfigure}
\caption{Square drawing - $\mathcal{H_{\infty}}$ optimal controller with ILC}
\label{fig:ILC_plotter_square_detail}
\end{figure}

The results are not great, the inherited coupling problem this simple design of ILC cannot handle. I tried several variants of filter $Q$, learning coefficients, but it did not help, so the figures are for consistency and comparison with the very same coefficients as before. Namely $ILC_{kp} = 0.1$ and 0/20 Hz filter. A different, more complex solution to this problem needs to be developed. There exist an older, but very interesting article of $\mathcal{H_{\infty}}$ synthesis of ILC controller itself \cite{goh:1996}. Different techniques with interresting results can be found at \cite{Zundert:2016}. But that is for the next time \ldots

