\chapter{Control software}
\label{chapter:ctrl_soft}
\section{Overview}
\subsection{Iterative Learning Control}

As mentioned briefly in the introduction, Iterative Learning Control is a type of feedforward regulator, which learns and takes advantage of previous iterations of the same task. It stores information about the regulation error and processes it in the next iteration to improve the signal. This implies the first design requirement, namely that the ILC controller must have memory and information about the iterations. I designed the ILC setup in such a way, that we have access to a time sample $k$ in the iteration and also total iteration number $j$, so it is easier for further development to implement even a higher-order ILC algorithms, which use information about the error evolution over more that one iteration from the past.

Let me state the basic math symbols used in relation to the ILC. Mostly, I will be following the notation of the Survey \cite{Survey:2006}, where $k$ is the time index, $j$ is the iteration index, $w_j$ is the ILC control contribution, $u_j$ is the plant input, $y_d$ is the demanded tracking reference, $y_j$ is the output and d is an exogenous signal which repeats each iteration. $L$ is the learning function of ILC, $Q$ is the output filter of ILC. $C$ is the standard controller in the feedback loop and finally, $G$ is the plant. The ILC setup in detail and in the parallel version, which I will use, can be seen in the fig. \ref{fig:ILC_setup}

Each iteration, the ILC algorithm uses the control error of previous iteration $j-1$ and updates signal $l_j$ via the matrix $L$. The $L$ is usually a P or PD learning function since I term is naturally achieved through the integration of errors over the trials. For a PD learning function, the mathematical equation is eq. \ref{eq:PD_learning_ILC}
\begin{equation}\label{eq:PD_learning_ILC}
l_j(k) = k_pe_{j-1}(k+1)+k_d(e_{j-1}(k+1)-e_{j-1}(k)),
\end{equation}
where $k_p$ is the proportional gain and $k_d$ is the derivative gain. 

The $l_j$ is then added to the previous $w-1$ and filtered with $Q$ to get
\begin{equation}\label{eq:PD_learning_ILC_complete}
w_j(k) = Q(w_{j-1}(k)+l_j(k))
\end{equation}

$Q$ is usually a low-pass filter, which has the purpose of disabling learning at high frequencies. That is desirable for added robustness, noise filtering and it could also help to achieve monotonic convergence. \ref{Survey:2006}

\begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{figs/ILC4}
    \caption{ILC setup}
    \label{fig:ILC_setup}
\end{figure}

\subsection{Matlab structure}
All the parameters are in the eponymous file \verb|parameters.m| in the root folder. Here the reference is generated, you can specify a sample time, parameters of the model, and all other simulation parameters. All the blocks and experiments are properly parametrized, so if you change any value here it should not break the consistency. 

For the purpose of comfortable usability of all the Simulink blocks, I created a Plotter library. It contains a bunch of blocks, some of them being a subset of another. I will describe the blocks in more details later in this chapter. The most important ones are
\begin{itemize}
\item Model of the plotter, unchanged original from my colleague
\item Reduced model of the plotter (for $\mathcal{H_{\infty}}$ controller design), which lacks the integrated outputs
\item Motor\_can block, which can be used for communication with the motor driver over CAN
\item ILC\_source, which generates the reference and control signals for ILC block
\item ILC\_v3, which is the block for iterative learning control feedforward regulator
\end{itemize}


\section{ILC implementation}
\subsection{ILC reference}
 Considering the previous, first I decided to create a dedicated Simulink block of reference and associated control signals. As parameters, the block takes an array of a single iteration of reference and a sampling time. Those I have parametrized and defined in the workspace of Matlab. The block with its outputs can be seen in the fig. \ref{fig:ILC_reference}.
 
 \begin{figure}[H]
    \centering
    \includegraphics[width=7cm]{figs/ILC_reference}
    \caption{ILC reference Simulink block}
    \label{fig:ILC_reference}
\end{figure}
 
The outputs have the following functionality - the \textit{Sample time $k$} is a time index of samples, which resets every iteration. The \textit{Repeating reference} simply takes the specified reference and repeats it over and over again. \textit{Iteration $j$} is a number of the currently ongoing reference iteration and lastly,  \textit{Length of a single iteration} output is actually a full signal of one reference iteration, which has the purpose of memory allocation in the main ILC controller. 

\subsection{ILC controller}
As a reference design and for verification, I designed a simple ILC implementing PD-type learning function (see eq. \ref{eq:PD_learning_ILC_complete}). It uses a memory two memory blocks, which stores information about previous iterations $w_{j-1}$ and $e_{j-1}$. The coefficients $ILC_{kd}$ and $ILC_{kp}$ can be set within the universal \verb|parameters.m| file. Inside the memory blocks, there is a simple Matlab function, which takes the sequence, copies it over and sends it back to itself via the Unit Delay with length initialization. That is why I needed the length signal, to initialize this $\frac{1}{z}$ memory. The ILC can be seen in the fig.  \ref{fig:ILC_reference}, with the inside on fig. \ref{fig:ILC_controller_inside}.

 \begin{figure}[H]
    \centering
    \includegraphics[width=5cm]{figs/ILC_v3}
    \caption{ILC reference Simulink block}
    \label{fig:ILC_reference}
\end{figure}

\begin{figure}[H]
  \makebox[\textwidth][c]{\includegraphics[width=1.1\textwidth]{figs/ILC_controller}}
  \caption{ILC controller Simulink block - inside}
  \label{fig:ILC_controller_inside}
\end{figure}

\subsection{ILC performance evaluator}
The performance of a controller can be done just by looking to a response, however, it's not a bad thing to actually quantify it. That is why a designed a simple quadratic evaluator block, which takes  $k$ and $e_j$. Its purpose is to square and sum up the control errors over an iteration. Then it holds this value on its output over the whole next iteration. Block is in the fig \ref{fig:evaluator}.

 \begin{figure}[H]
    \centering
    \includegraphics[width=5cm]{figs/evaluator}
    \caption{Quadratic evaluator}
    \label{fig:evaluator}
\end{figure}





\subsection{ILC verification and experiments}
\label{section:ILC_verification}
To verify both the generator of reference and other control signals and the ILC controller, I have done some experiments. First I used a SISO $1^{st}$ order system  with one of the simplest transfer functions $G(s) = \frac{1}{s+1}$. For a feedback controller $C$ a PD controller was used, with coefficients estimated at the first attempt to $K_p = 3$, $K_d = 0.1$. The learning function of the ILC was set similarly elementary to  $ILC_{kp} = 0.1$ and $ILC_{kd} = 0$. A filter $Q$ is a simple running mean, window of 4 samples. The result can be observed in the fig. \ref{fig:ILC_1st}.

\begin{figure}[H] 
  \begin{subfigure}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=1\linewidth]{figs/ILC_1st_8secs} 
    \caption{First 8 iterations} 
    \label{fig:ILC_1st:a} 
    \vspace{4ex}
  \end{subfigure}%% 
  \begin{subfigure}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=1\linewidth]{figs/ILC_1st_10-14s} 
    \caption{Around iteration 15} 
    \label{fig:ILC_1st:b} 
    \vspace{4ex}
  \end{subfigure} 
  \begin{subfigure}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=1\linewidth]{figs/ILC_1st_last_3s} 
    \caption{After 100 iterations} 
    \label{fig:ILC_1st:c} 
  \end{subfigure}%%
  \begin{subfigure}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=1\linewidth]{figs/ILC_1st_last_3s_detail} 
    \caption{After 100 iterations - detail} 
    \label{fig:ILC_1st:d} 
  \end{subfigure} 
  \caption{ILC first experiment}
  \label{fig:ILC_1st} 
\end{figure}

As we can see, in this scenario ILC converges really nicely. At the beginning, the poorly tuned PD regulator lags and do not even closely reach the desired trajectory, but ILC and its feedforward action quickly takes over \ref{fig:ILC_1st:a}. Around iteration 4 the tracking is pretty nice, but it still lags. For a small period (around 5 iterations) the ILC begins to slightly overshoot, but just around iteration 15, the tracking stabilizes \ref{fig:ILC_1st:b}. Around iteration 100 the tracking is nearly perfect, as we can  see on \ref{fig:ILC_1st:c}  or in detail \ref{fig:ILC_1st:d}. You may notice, that the controller seemingly is not causal and predicts the future, as the output begins to decrease before the reference. That is the additional information from past iterations. 

Let's see, how the ILC performs when the evaluation criterion is a squared control error. Using the quadratic evaluator, we can observe a nice monotonic convergence. The curve is on fig. \ref{fig:evaluator}, please note the y-axis in logarithmic scale.

 \begin{figure}[H]
    \centering
    \includegraphics[width=15cm]{figs/ILC_1st_eval}
    \caption{Quadratic performance, sawtooth reference}
    \label{fig:evaluator}
\end{figure}

However, not with every scenario ILC works this smoothly, at least for a first try. 
%In the next experiment, I will show one experiment, that I wish I performed earlier.
Let's now try exactly the same setup, but a rectangular reference. 

\begin{figure}[H] 
  \begin{subfigure}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=1\linewidth]{figs/ILC_rectangle_1} 
    \caption{First 5 iterations} 
    \label{fig:ILC_rectangle:a} 
    \vspace{4ex}
  \end{subfigure}%% 
  \begin{subfigure}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=1\linewidth]{figs/ILC_rectangle_2} 
    \caption{Stady state after 25 iterations} 
    \label{fig:ILC_rectangle:b} 
    \vspace{4ex}
  \end{subfigure} 
  \caption{ILC rectangle response, with moving average Q filter}
  \label{fig:ILC_rectangle1} 
 \end{figure}
 The response looks alright only the first two iterations, then it overshoots way too much. And it does not get better, in fact, the quadratic error at one point even begins to rise. It looks like ILC tries to compensate for the error a little too much. However, adjusting the learning coefficient $ILC_{kp}$, or introduction of a derivative component does not help, it can only make the problem appear sooner or later. What we need is to tune the $Q$ filter. I designed $Q$ in Filter Designer utility, so it has $F_{pass} = \SI{0}{\hertz}$, $F_{stop} = \SI{20}{\hertz}$, and attenuation magnitude of 30 dB, so it has a reasonable order of 57. After these adjustments, our system produces following response (fig. \ref{fig:ILC_rectangle2})
 
 \begin{figure}[H] 
  \begin{subfigure}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=1\linewidth]{figs/ILC_rectangle_3} 
    \caption{First 5 iterations} 
    \label{fig:ILC_rectangle:c} 
  \end{subfigure}%%
  \begin{subfigure}[b]{0.5\linewidth}
    \centering
    \includegraphics[width=1\linewidth]{figs/ILC_rectangle_4} 
    \caption{Stady state after 25 iterations} 
    \label{fig:ILC_rectangle:d} 
  \end{subfigure} 
  \caption{ILC rectangle response, with advanced Q filter}
  \label{fig:ILC_rectangle2} 
\end{figure}

On the performance evaluation, we can observe the non-montonic convergence of ILC with moving average filter (fig. \ref{fig:evaluator2}). At one point, the performance begins to be worse. With more advanced filtering, the performance curve looks better, although in absolute value in the steady state it is worse (fig. \ref{fig:evaluator3}). That is because with the better filtering I effectively lowered the ILC contribution to the control. That is a price for suppressing the excessive overshoots.


 \begin{figure}[H]
    \centering
    \includegraphics[width=15cm]{figs/ILC_rectangle_quadratic_1}
    \caption{Quadratic performance, rectangle reference, moving average filter}
    \label{fig:evaluator2}
\end{figure}

 \begin{figure}[H]
    \centering
    \includegraphics[width=15cm]{figs/ILC_rectangle_quadratic_2}
    \caption{Quadratic performance, rectangle reference, advanced filter}
    \label{fig:evaluator3}
\end{figure}



